import React, { useEffect, useState, useRef } from 'react';
import {
    StyleSheet, Text, View, ScrollView, Button, Image, Dimensions
} from 'react-native';
import 'intl';
import 'intl/locale-data/jsonp/en';

// Project API key.
const projectId = 'HTNIg)njFTVmk)5T7jc_';
const projectLocale = 'en';

export default function App() {
    // Current state JSON object.
    const apiState = useRef({});
    // Current API page.
    const apiPage = useRef({});

    // Holds the current page state data.

    // Page description.
    const [pageDescription, setPageDescription] = useState('');
    // Page questions array.
    const [questions, setQuestions] = useState([]);
    // Recommendations array.
    const [recommendations, setRecommendations] = useState([]);
    // Should the back button be displayed.
    const [canReturn, setCanReturn] = useState(false);
    // Should the next button be displayed.
    const [canAdvance, setCanAdvance] = useState(false);

    /**
     * Helper method to POST JSON data to the API server.
     */
    function post(data) {
        return fetch('https://api.cartographer.drivecommerce.com/api/v4/runtime/conversations', {
            method: 'POST',
            cache: 'no-store',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((response) => response.json());
    }

    /**
     * Update the quiz state according to current selections.
     */
    function updateQuiz() {
        post(apiState.current).then((data) => {
            processPages(data);
        });
    }

    /**
     * Interpret API response and build quiz page UX.
     */
    function processPages(data) {
        // Store current API state object.
        apiState.current = data;

        // The last page in the stack is the current one.
        apiPage.current = data.pages[data.pages.length - 1];

        // Build page UX.
        setPageDescription(apiPage.current.description);
        setCanReturn(apiPage.current.canReturn);
        setCanAdvance(apiPage.current.canAdvance);
        setQuestions(apiPage.current.questions);

        // Make a nicely formatted currency values before updating the state.
        if (apiState.current.recommendations) {
            apiState.current.recommendations.recommendations.forEach((recommendation) => {
                const { product } = recommendation;
                if (product && product.price && product.priceCurrency) {
                    product.priceFormatted = new Intl.NumberFormat(apiState.current.locale, {
                        style: 'currency',
                        currency: product.priceCurrency,
                    }).format(product.price);
                }
            });

            setRecommendations(apiState.current.recommendations.recommendations);
        } else {
            setRecommendations(null);
        }
    }

    // Begin loading - start the conversation.
    useEffect(() => {
        // To begin we only need to know the project API key, either for Production or Draft mode.
        post({
            project: projectId,
            locale: projectLocale,
        }).then((data) => {
            processPages(data);
        });
    }, []);

    /**
     * Selects one choice in a question, while deactivating the rest.
     */
    function toggleSingleChoice(question, choice) {
        question.choices.forEach((anotherChoice) => {
            // eslint-disable-next-line no-param-reassign
            anotherChoice.selected = false;
        });

        // eslint-disable-next-line no-param-reassign
        choice.selected = true;

        // Update the quiz state.
        updateQuiz();
    }

    /**
     * Toggles a choice selected state without affecting other choices.
     */
    function toggleChoice(question, choice) {
        // eslint-disable-next-line no-param-reassign
        choice.selected = !choice.selected;

        // Update the quiz state.
        updateQuiz();
    }

    /**
     * Navigates back in the stack of pages.
     */
    function toggleBack() {
        // Simply remove the last page from the stack.
        apiState.current.pages = apiState.current.pages.slice(0, apiState.current.pages.length - 1);

        // Also update now-the-last page by marking it incomplete.
        const lastPage = apiState.current.pages[apiState.current.pages.length - 1];

        lastPage.complete = false;

        // Also undoing previous choice selections.
        // Depending on UX requirements, it might be desired to keep these selected. If that
        // is the case, and the page is selected to autoadvance, we recommend providing an
        // explicit Next button.
        lastPage.questions.forEach((question) => {
            if (question.choices) {
                question.choices.forEach((choice) => {
                    // eslint-disable-next-line no-param-reassign
                    choice.selected = false;
                });
            }
        });

        // Update the quiz state.
        updateQuiz();
    }

    /**
     * Navigates forward in the stack.
     */
    function toggleNext() {
        // If page completion conditions have been met, mark the page as complete.
        if (apiPage.current.canAdvance) {
            // This happens automatically is the page is marked to autoadvance in the backend UI.
            apiPage.current.complete = true;
        }

        // Update the quiz state.
        updateQuiz();
    }

    /**
     * Displays a question as a set of buttons, only one of which can be selected at a time.
     */
    function QuestionButtons({ question }) {
        return (
            <View>
                {question.choices.map((choice) => (
                    <View key={choice.choice} style={styles.choiceButton}>
                        {/* In the real world scenario you'd probably use TouchableOpacity. */}
                        {/* Adjust the button color depending on the selected state. */}
                        <Button
                            title={choice.description}
                            color={choice.selected ? '#000' : null}
                            onPress={() => toggleSingleChoice(question, choice)}
                        />
                    </View>
                ))}
            </View>
        );
    }

    /**
     * Displays a question as a set of buttons, multiple selectios are supported.
     */
    function QuestionMultiSelectButtons({ question }) {
        return (
            <View>
                {question.choices.map((choice) => (
                    <View key={choice.choice} style={styles.choiceButton}>
                        {/* Adjust the button color depending on the selected state. */}
                        <Button
                            title={choice.description}
                            color={choice.selected ? '#000' : null}
                            onPress={() => toggleChoice(question, choice)}
                        />
                    </View>
                ))}
            </View>
        );
    }

    /**
     * Displays the question body UX depending on the desired presentation type.
     */
    function QuestionBody({ question }) {
        if (question.showAs === 'Buttons') {
            return <QuestionButtons question={question} />;
        }

        if (question.showAs === 'MultiSelect') {
            return <QuestionMultiSelectButtons question={question} />;
        }

        return null;
    }

    /**
     * Displays recommendations.
     */
    function Recommendations({ products }) {
        return (
            <View style={styles.recommendations}>
                {products.map((recommendation) => (
                    <View key={recommendation.product.externalId} style={styles.product}>
                        {/* Draw the product image if there is any */}
                        {recommendation.product.defaultImageUrl && (
                            <Image
                                style={styles.productImage}
                                source={{
                                    uri: recommendation.product.defaultImageUrl.replace(/^\/\//, 'https://'),
                                }}
                            />
                        )}
                        {/* Just like quiz questions and answers, product data will be automatically
                            pulled for the appropriate locale.
                        */}
                        <Text style={styles.productTitle}>{recommendation.product.name}</Text>
                        <Text style={styles.productPrice}>
                            {recommendation.product.priceFormatted}
                        </Text>
                        {/* You might need HTML rendering component if your product data
                            may contain rich content
                        */}
                        <Text style={styles.productDescription}>
                            {recommendation.product.description}
                        </Text>
                    </View>
                ))}
            </View>
        );
    }

    return (
        <ScrollView style={styles.container} contentContainerStyle={styles.pageContent}>
            {/* Scroll content if it gets too long, for example if there are multiple questions
                per page, or we are on results page
            */}

            {/* Display page title
                Descriptions are automatically localized according to the locale setting
            */}

            <View style={styles.pageDescriptionContent}>
                <Text style={styles.pageDescription}>{pageDescription}</Text>
            </View>

            {/* Loop through questions and build out questions UI */}
            {questions.map((question) => (
                <View style={styles.question} key={question.question}>
                    <Text style={styles.questionDescription}>{question.description}</Text>

                    <QuestionBody question={question} />
                </View>
            ))}

            {/* Show recommendations */}
            {recommendations && (
                <Recommendations products={recommendations} />
            )}

            {/* Show navigation buttons */}
            <View style={styles.pageNavigation}>
                {canReturn && (
                    <Button
                        title="Back"
                        onPress={() => toggleBack()}
                    />
                )}

                {canAdvance && (
                    <Button
                        title="Next"
                        onPress={() => toggleNext()}
                    />
                )}
            </View>
        </ScrollView>
    );
}

const windowDimensions = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    },

    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },

    pageContent: {
        paddingTop: 65,
        paddingBottom: 16,
        flexGrow: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
    },

    pageDescriptionContent: {
        position: 'absolute',
        width: '100%',
        left: 0,
        top: 60,
    },

    pageDescription: {
        color: '#666',
        fontSize: 30,
        textAlign: 'center',
    },

    question: {
    },

    questionDescription: {
        marginBottom: 16,
        color: '#000',
        fontSize: 26,
        textAlign: 'center',
    },

    choiceButton: {
        marginVertical: 8,
        marginHorizontal: 8,
    },

    pageNavigation: {
        marginTop: 16,
        marginHorizontal: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    recommendations: {
        marginVertical: 16,
        marginHorizontal: 8,
    },

    product: {
        marginVertical: 8,
    },

    productImage: {
        width: windowDimensions.width - 16,
        height: windowDimensions.width - 16,
        resizeMode: 'contain',
    },

    productTitle: {
        color: '#000',
        fontSize: 20,
    },

    productPrice: {
        marginVertical: 8,
        color: '#999',
        fontSize: 18,
    },

    productDescription: {
        color: '#999',
        fontSize: 12,
    },
});
